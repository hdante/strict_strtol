/*
 * Copyright © 2020 Henrique Dante de Almeida
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "strict_strtol.h"

int main(void)
{
	char buff[100];
	char *input, *endptr;
	long value;

	while (1) {
		printf("Input a number: ");
		fflush(stdout);
		input = fgets(buff, sizeof(buff), stdin);
		if (!input) {
			putchar('\n');
			return 0;
		}

		if (strict_strtol(input, &endptr, 0, &value,
				  STRTOL_ALLOW_WHITESPACE |
				  STRTOL_EXTENDED_BASE0 |
				  STRTOL_DISABLE_OCTAL0)) {
			printf("The number is: %ld\n", value);
		}
		else {
			printf("Error at position %td: %s\n",
			       endptr-input, strerror(errno));
		}
	}
}
