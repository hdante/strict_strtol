# strict_strtol - A stricter version of strtol()

strict_strtol is a tiny library that implements strict versions of the
standard C functions strtol() and family. It's meant to be compiled to a
static library or directly copied inside other projects for direct
usage.

Supported platforms: strict_strtol only uses standard ISO C and is
portable to any platform. It's tested on Linux, MacOS and Windows.

Build requirements:

- C compiler
- meson build system
- ninja build system (or equivalent)
- (optional) asciidoc, for manpages

Example Ubuntu requirements installation:

	$ sudo apt-get install gcc meson ninja-build asciidoc

Build instructions:

	$ meson build
	$ ninja -C build
	$ ninja -C build test
	$ ninja -C build install

Build with additional options (build as shared library, release build):

	$ meson -Ddefault_library=shared -Dbuildtype=release build
	$ ninja -C build

Usage example:

	$ cat examples/read_number.c
	(...)
	#include <errno.h>
	#include <stdio.h>
	#include <string.h>

	#include "strict_strtol.h"

	int main(void)
	{
		char buff[100];
		char *input, *endptr;
		long value;

		while (1) {
			printf("Input a number: ");
			fflush(stdout);
			input = fgets(buff, sizeof(buff), stdin);
			if (!input) {
				putchar('\n');
				return 0;
			}

			if (strict_strtol(input, &endptr, 0, &value,
					  STRTOL_ALLOW_WHITESPACE |
					  STRTOL_EXTENDED_BASE0 |
					  STRTOL_DISABLE_OCTAL0)) {
				printf("The number is: %ld\n", value);
			}
			else {
				printf("Error at position %td: %s\n",
				       endptr-input, strerror(errno));
			}
		}
	}
	$ cc -o read_number examples/read_number.c -lstrict_strtol
	$ ./read_number
	Input a number: 123456
	The number is: 123456
	Input a number: -0x300
	The number is: -768
	Input a number:             0b1101
	The number is: 13
	Input a number: 123a34
	Error at position 3: Numerical argument out of domain
	Input a number: 123 456
	Error at position 4: Bad message
	Input a number: 9223372036854775809
	Error at position 18: Value too large for defined data type
	Input a number:
	$

For short strict_strtol() fixes strtol() lack of compliance of the
[principle of least astonishment][1]: in the above examples, strtol()
would return 123 when parsing "123a34" and strict_strtol() returns
error. When parsing "123 456" strtol() would return 123 and
strict_strtol() returns error. strtol() would also return zero on empty
input, while strict_strtol() returns error. Finally, proper handling
of strtol() errors requires first setting errno to zero, then calling
strtol(), then checking errno, while strict_strtol() always returns
success or failure.

strict_strtol() also supports parsing the binary and octal prefixes 0b
and 0o with the appropriate flag and also disables interpreting the 0
prefix as octal with the appropriate flag (so that 0123 is parsed as 123
instead of 83). Also, with the appropriate flags, leading and trailing
whitespace can prohibited, making it easy to implement stricter message
parsers. There's a flag that allows strict_strtol() to succeed on
overflow (like strtol(), it always returns the saturated value anyway).

[1]: https://en.wikipedia.org/wiki/Principle_of_least_astonishment


License: strict_strtol() is free software, licensed with the ISC/OpenBSD
license.
