/*
 * Copyright © 2020 Henrique Dante de Almeida
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#define _XOPEN_SOURCE 700
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdbool.h>

#include <stdio.h>

#include "strict_strtol.h"

#ifndef countof
#define countof(x) (sizeof(x)/sizeof(x[0]))
#endif

static long digit_value(char c)
{
	if (c >= '0' && c <= '9')
		return (c-'0');

	/* Don't rely on the compiler to optimize the switch to the trivial range
	 * condition on contiguous character sets and don't pay the price of allocating
	 * the lookup table either if we can avoid it. */
	#if ('a' + 1 == 'b' && 'b' + 1 == 'c' && 'c' + 1 == 'd' && 'd' + 1 == 'e' && \
	     'e' + 1 == 'f' && 'f' + 1 == 'g' && 'g' + 1 == 'h' && 'h' + 1 == 'i' && \
	     'i' + 1 == 'j' && 'j' + 1 == 'k' && 'k' + 1 == 'l' && 'l' + 1 == 'm' && \
	     'm' + 1 == 'n' && 'n' + 1 == 'o' && 'o' + 1 == 'p' && 'p' + 1 == 'q' && \
	     'q' + 1 == 'r' && 'r' + 1 == 's' && 's' + 1 == 't' && 't' + 1 == 'u' && \
	     'u' + 1 == 'v' && 'v' + 1 == 'w' && 'w' + 1 == 'x' && 'x' + 1 == 'y' && \
	     'y' + 1 == 'z' && \
	     'A' + 1 == 'B' && 'B' + 1 == 'C' && 'C' + 1 == 'D' && 'D' + 1 == 'E' && \
	     'E' + 1 == 'F' && 'F' + 1 == 'G' && 'G' + 1 == 'H' && 'H' + 1 == 'I' && \
	     'I' + 1 == 'J' && 'J' + 1 == 'K' && 'K' + 1 == 'L' && 'L' + 1 == 'M' && \
	     'M' + 1 == 'N' && 'N' + 1 == 'O' && 'O' + 1 == 'P' && 'P' + 1 == 'Q' && \
	     'Q' + 1 == 'R' && 'R' + 1 == 'S' && 'S' + 1 == 'T' && 'T' + 1 == 'U' && \
	     'U' + 1 == 'V' && 'V' + 1 == 'W' && 'W' + 1 == 'X' && 'X' + 1 == 'Y' && \
	     'Y' + 1 == 'Z')
	if (c >= 'a' && c <= 'z')
		return (c-'a'+10);

	if (c >= 'A' && c <= 'Z')
		return (c-'A'+10);
	#else
	switch(c) {
	case 'a':
	case 'A':
		return 10;
	case 'b':
	case 'B':
		return 11;
	case 'c':
	case 'C':
		return 12;
	case 'd':
	case 'D':
		return 13;
	case 'e':
	case 'E':
		return 14;
	case 'f':
	case 'F':
		return 15;
	case 'g':
	case 'G':
		return 16;
	case 'h':
	case 'H':
		return 17;
	case 'i':
	case 'I':
		return 18;
	case 'j':
	case 'J':
		return 19;
	case 'k':
	case 'K':
		return 20;
	case 'l':
	case 'L':
		return 21;
	case 'm':
	case 'M':
		return 22;
	case 'n':
	case 'N':
		return 23;
	case 'o':
	case 'O':
		return 24;
	case 'p':
	case 'P':
		return 25;
	case 'q':
	case 'Q':
		return 26;
	case 'r':
	case 'R':
		return 27;
	case 's':
	case 'S':
		return 28;
	case 't':
	case 'T':
		return 29;
	case 'u':
	case 'U':
		return 30;
	case 'v':
	case 'V':
		return 31;
	case 'w':
	case 'W':
		return 32;
	case 'x':
	case 'X':
		return 33;
	case 'y':
	case 'Y':
		return 34;
	case 'z':
	case 'Z':
		return 35;
	default:
		break;
	}
	#endif

	return -1;
}

bool strict_strtol(const char *RESTRICT str, char **RESTRICT endptr, int base,
		   long *value, enum strtol_flags flags)
{
	bool empty, neg;
	bool ws, ext, disoct, sat, bound;
	char prefix;
	int err;
	long v, d;
	size_t i;

	static const struct {
		int base;
		char prefix;
		bool ext;
	} prefixes[] = {
		{ 2, 'b', true },
		{ 2, 'B', true },
		{ 8, 'o', true },
		{ 8, 'O', true },
		{ 16, 'x', false },
		{ 16, 'X', false }
	};

	assert(str != NULL);
	assert(value != NULL);

	/* Check for invalid base paramenters and return early */
	if (base < 0 || base == 1 || base > 36) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = EINVAL;
		return false;
	}

	ws = flags & STRTOL_ALLOW_WHITESPACE;
	ext = flags & STRTOL_EXTENDED_BASE0;
	disoct = flags & STRTOL_DISABLE_OCTAL0;
	sat = flags & STRTOL_ALLOW_SATURATE;
	bound = flags & STRTOL_BOUNDED_PARSE;

	/* Check for invalid flag combinations */
	if (ws && bound) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = EINVAL;
		return false;
	}

	empty = true;
	v = 0;
	err = 0;

	/* Skip leading whitespace */
	if (ws) {
		while (isspace(*str))
			str++;
	}

	/* Check sign */
	if (*str == '-') {
		neg = true;
		str++;
	}
	else {
		neg = false;
		if (*str == '+')
			str++;
	}

	/* Resolve base and skip base prefixes */
	if (base == 0 && *str != '0')
		base = 10;
	else if (*str == '0') {
		prefix = str[1];

		for (i = 0; i < countof(prefixes); i++) {
			if (prefixes[i].prefix == prefix && (ext || !prefixes[i].ext)) {
				if (base == 0)
					base = prefixes[i].base;

				if (base == prefixes[i].base) {
					str += 2;
					break;
				}
			}
		}

		if (base == 0) {
			if (disoct)
				base = 10;
			else
				base = 8;
		}
	}

	/* Convert */
	while (*str != '\0' && !isspace(*str)) {
		d = digit_value(*str);
		if (d < 0) {
			err = EBADMSG;
			break;
		}

		if (d >= base) {
			err = EDOM;
			break;
		}

		if (bound && !empty && v == 0 && d == 0) {
			err = EBADMSG;
			break;
		}

		if (err != EOVERFLOW) {
			if (neg) {
				if (v < (LONG_MIN+d)/base) {
					err = EOVERFLOW;
					v = LONG_MIN;
					if (!sat) break;
				}
				else
					v = v*base - d;
			}
			else {
				if (v > (LONG_MAX-d)/base) {
					err = EOVERFLOW;
					v = LONG_MAX;
					if (!sat) break;
				}
				else
					v = v*base + d;
			}
		}

		empty = false;
		str++;
	}

	/* Overflow errors also store a value (either the minimum or maximum) */
	if (err == EOVERFLOW) {
	       if (sat)
		       err = 0;
	       else
		       *value = v;
	}

	/* Check if conversion was interrupted */
	if (err) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = err;
		return false;
	}

	/* Check if empty */
	if (empty) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = EBADMSG;
		return false;
	}

	/* Skip trailing whitespace */
	if (ws) {
		while (isspace(*str))
			str++;
	}

	if (endptr != NULL) *endptr = (char *)str;

	/* Check if there's trailing garbage */
	if (*str != '\0') {
		errno = EBADMSG;
		return false;
	}

	*value = v;

	return true;
}

bool strict_strtoul(const char *RESTRICT str, char **RESTRICT endptr, int base,
		    unsigned long *value, enum strtol_flags flags)
{
	bool empty;
	bool ws, ext, disoct, sat, bound;
	char prefix;
	int err;
	size_t i;
	unsigned long v;
	int d;

	static const struct {
		int base;
		char prefix;
		bool ext;
	} prefixes[] = {
		{ 2, 'b', true },
		{ 2, 'B', true },
		{ 8, 'o', true },
		{ 8, 'O', true },
		{ 16, 'x', false },
		{ 16, 'X', false }
	};

	assert(str != NULL);
	assert(value != NULL);

	/* Check for invalid base paramenters and return early */
	if (base < 0 || base == 1 || base > 36) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = EINVAL;
		return false;
	}

	ws = flags & STRTOL_ALLOW_WHITESPACE;
	ext = flags & STRTOL_EXTENDED_BASE0;
	disoct = flags & STRTOL_DISABLE_OCTAL0;
	sat = flags & STRTOL_ALLOW_SATURATE;
	bound = flags & STRTOL_BOUNDED_PARSE;

	/* Check for invalid flag combinations */
	if (ws && bound) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = EINVAL;
		return false;
	}

	empty = true;
	v = 0;
	err = 0;

	/* Skip leading whitespace */
	if (ws) {
		while (isspace(*str))
			str++;
	}

	/* Check sign */
	if (*str == '+')
		str++;

	/* Resolve base and skip base prefixes */
	if (base == 0 && *str != '0')
		base = 10;
	else if (*str == '0') {
		prefix = str[1];

		for (i = 0; i < countof(prefixes); i++) {
			if (prefixes[i].prefix == prefix && (ext || !prefixes[i].ext)) {
				if (base == 0)
					base = prefixes[i].base;

				if (base == prefixes[i].base) {
					str += 2;
					break;
				}
			}
		}

		if (base == 0) {
			if (disoct)
				base = 10;
			else
				base = 8;
		}
	}

	/* Convert */
	while (*str != '\0' && !isspace(*str)) {
		d = digit_value(*str);
		if (d < 0) {
			err = EBADMSG;
			break;
		}

		if (d >= base) {
			err = EDOM;
			break;
		}

		if (bound && !empty && v == 0 && d == 0) {
			err = EBADMSG;
			break;
		}

		if (err != EOVERFLOW) {
			if (v > (ULONG_MAX-(unsigned long)d)/(unsigned long)base) {
				err = EOVERFLOW;
				v = ULONG_MAX;
				if (!sat) break;
			}
			else
				v = v*base + (unsigned long)d;
		}

		empty = false;
		str++;
	}

	/* Overflow errors also store a value (either the minimum or maximum) */
	if (err == EOVERFLOW) {
	       if (sat)
		       err = 0;
	       else
		       *value = v;
	}

	/* Check if conversion was interrupted */
	if (err) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = err;
		return false;
	}

	/* Check if empty */
	if (empty) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = EBADMSG;
		return false;
	}

	/* Skip trailing whitespace */
	if (ws) {
		while (isspace(*str))
			str++;
	}

	if (endptr != NULL) *endptr = (char *)str;

	/* Check if there's trailing garbage */
	if (*str != '\0') {
		errno = EBADMSG;
		return false;
	}

	*value = v;

	return true;
}

bool strict_strtoull(const char *RESTRICT str, char **RESTRICT endptr, int base,
		    unsigned long long *value, enum strtol_flags flags)
{
	bool empty;
	bool ws, ext, disoct, sat, bound;
	char prefix;
	int err;
	size_t i;
	unsigned long long v;
	int d;

	static const struct {
		int base;
		char prefix;
		bool ext;
	} prefixes[] = {
		{ 2, 'b', true },
		{ 2, 'B', true },
		{ 8, 'o', true },
		{ 8, 'O', true },
		{ 16, 'x', false },
		{ 16, 'X', false }
	};

	assert(str != NULL);
	assert(value != NULL);

	/* Check for invalid base paramenters and return early */
	if (base < 0 || base == 1 || base > 36) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = EINVAL;
		return false;
	}

	ws = flags & STRTOL_ALLOW_WHITESPACE;
	ext = flags & STRTOL_EXTENDED_BASE0;
	disoct = flags & STRTOL_DISABLE_OCTAL0;
	sat = flags & STRTOL_ALLOW_SATURATE;
	bound = flags & STRTOL_BOUNDED_PARSE;

	/* Check for invalid flag combinations */
	if (ws && bound) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = EINVAL;
		return false;
	}

	empty = true;
	v = 0;
	err = 0;

	/* Skip leading whitespace */
	if (ws) {
		while (isspace(*str))
			str++;
	}

	/* Check sign */
	if (*str == '+')
		str++;

	/* Resolve base and skip base prefixes */
	if (base == 0 && *str != '0')
		base = 10;
	else if (*str == '0') {
		prefix = str[1];

		for (i = 0; i < countof(prefixes); i++) {
			if (prefixes[i].prefix == prefix && (ext || !prefixes[i].ext)) {
				if (base == 0)
					base = prefixes[i].base;

				if (base == prefixes[i].base) {
					str += 2;
					break;
				}
			}
		}

		if (base == 0) {
			if (disoct)
				base = 10;
			else
				base = 8;
		}
	}

	/* Convert */
	while (*str != '\0' && !isspace(*str)) {
		d = digit_value(*str);
		if (d < 0) {
			err = EBADMSG;
			break;
		}

		if (d >= base) {
			err = EDOM;
			break;
		}

		if (bound && !empty && v == 0 && d == 0) {
			err = EBADMSG;
			break;
		}

		if (err != EOVERFLOW) {
			if (v > (ULLONG_MAX-(unsigned long long)d)/
			    (unsigned long long)base) {
				err = EOVERFLOW;
				v = ULLONG_MAX;
				if (!sat) break;
			}
			else
				v = v*base + (unsigned long long)d;
		}

		empty = false;
		str++;
	}

	/* Overflow errors also store a value (either the minimum or maximum) */
	if (err == EOVERFLOW) {
	       if (sat)
		       err = 0;
	       else
		       *value = v;
	}

	/* Check if conversion was interrupted */
	if (err) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = err;
		return false;
	}

	/* Check if empty */
	if (empty) {
		if (endptr != NULL) *endptr = (char *)str;
		errno = EBADMSG;
		return false;
	}

	/* Skip trailing whitespace */
	if (ws) {
		while (isspace(*str))
			str++;
	}

	if (endptr != NULL) *endptr = (char *)str;

	/* Check if there's trailing garbage */
	if (*str != '\0') {
		errno = EBADMSG;
		return false;
	}

	*value = v;

	return true;
}
