/*
 * Copyright © 2020 Henrique Dante de Almeida
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef STRICT_STRTOL_H
#define STRICT_STRTOL_H

#include <stdbool.h>

#if __GNUC__ >= 4
#pragma GCC visibility push(default)
#endif

#if defined(__cplusplus)
extern "C" {
#endif

#ifndef RESTRICT
#if __STDC_VERSION__ >= 199901L
#define RESTRICT restrict
#else
#define RESTRICT
#endif
#endif

enum strtol_flags {
	STRTOL_ALLOW_WHITESPACE = 0x1,
	STRTOL_EXTENDED_BASE0 = 0x2,
	STRTOL_DISABLE_OCTAL0 = 0x4,
	STRTOL_ALLOW_SATURATE = 0x8,
	STRTOL_BOUNDED_PARSE = 0x10
};

bool strict_strtol(const char *RESTRICT str, char **RESTRICT endptr, int base,
                   long *value, enum strtol_flags flags);
bool strict_strtoul(const char *RESTRICT str, char **RESTRICT endptr, int base,
                    unsigned long *value, enum strtol_flags flags);
bool strict_strtoull(const char *RESTRICT str, char **RESTRICT endptr, int base,
                     unsigned long long*value, enum strtol_flags flags);

#if defined(__cplusplus)
} /* extern "C" */
#endif

#if __GNUC__ >= 4
#pragma GCC visibility pop
#endif

#endif /* STRICT_STRTOL_H */
