/*
 * Copyright © 2020 Henrique Dante de Almeida
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#define _XOPEN_SOURCE 700
#undef NDEBUG
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "strict_strtol.h"

#ifndef countof
#define countof(x) (sizeof(x)/sizeof(x[0]))
#endif

#ifndef BUFF_SIZE
#define BUFF_SIZE 100
#endif

struct test {
	const char *input;
	int base;
	enum strtol_flags flags;
	bool result;
	unsigned long value;
	ptrdiff_t endptr;
	int err;
};

static const unsigned W = STRTOL_ALLOW_WHITESPACE;
static const unsigned E = STRTOL_EXTENDED_BASE0;
static const unsigned O = STRTOL_DISABLE_OCTAL0;
static const unsigned B = STRTOL_BOUNDED_PARSE;

static struct test tests[] = {
	/* input	base	flags	result	value		endptr	err */
	{ "0",		0,	0,   	true,	0,		1,	0 },
	{ "0",		2,	0,   	true,	0,		1,	0 },
	{ "0",		3,	0,   	true,	0,		1,	0 },
	{ "0",		4,	0,   	true,	0,		1,	0 },
	{ "0",		5,	0,   	true,	0,		1,	0 },
	{ "0",		6,	0,   	true,	0,		1,	0 },
	{ "0",		7,	0,   	true,	0,		1,	0 },
	{ "0",		8,	0,   	true,	0,		1,	0 },
	{ "0",		9,	0,   	true,	0,		1,	0 },
	{ "0",		10,	0,   	true,	0,		1,	0 },
	{ "0",		11,	0,   	true,	0,		1,	0 },
	{ "0",		12,	0,   	true,	0,		1,	0 },
	{ "0",		13,	0,   	true,	0,		1,	0 },
	{ "0",		14,	0,   	true,	0,		1,	0 },
	{ "0",		15,	0,   	true,	0,		1,	0 },
	{ "0",		16,	0,   	true,	0,		1,	0 },
	{ "0",		17,	0,   	true,	0,		1,	0 },
	{ "0",		18,	0,   	true,	0,		1,	0 },
	{ "0",		19,	0,   	true,	0,		1,	0 },
	{ "0",		20,	0,   	true,	0,		1,	0 },
	{ "0",		21,	0,   	true,	0,		1,	0 },
	{ "0",		22,	0,   	true,	0,		1,	0 },
	{ "0",		23,	0,   	true,	0,		1,	0 },
	{ "0",		25,	0,   	true,	0,		1,	0 },
	{ "0",		26,	0,   	true,	0,		1,	0 },
	{ "0",		27,	0,   	true,	0,		1,	0 },
	{ "0",		28,	0,   	true,	0,		1,	0 },
	{ "0",		29,	0,   	true,	0,		1,	0 },
	{ "0",		30,	0,   	true,	0,		1,	0 },
	{ "0",		31,	0,   	true,	0,		1,	0 },
	{ "0",		32,	0,   	true,	0,		1,	0 },
	{ "0",		33,	0,   	true,	0,		1,	0 },
	{ "0",		34,	0,   	true,	0,		1,	0 },
	{ "0",		35,	0,   	true,	0,		1,	0 },
	{ "0",		36,	0,   	true,	0,		1,	0 },
	{ "0",		1,	0,   	false,	0,		0,	EINVAL },
	{ "0",		37,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-1,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-2,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-3,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-4,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-5,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-6,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-7,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-8,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-9,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-10,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-11,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-12,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-13,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-14,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-15,	0,   	false,	0,		0,	EINVAL },
	{ "0",		-16,	0,   	false,	0,		0,	EINVAL },
	{ "0",		INT_MIN,0,   	false,	0,		0,	EINVAL },
	{ "0",		INT_MAX,0,   	false,	0,		0,	EINVAL },
	{ "1",		0,	0,   	true,	1,		1,	0 },
	{ "1",		1,	0,   	false,	0,		0,	EINVAL },
	{ "1",		-10,	0,   	false,	0,		0,	EINVAL },
	{ "1",		10,	0,   	true,	1,		1,	0 },
	{ "1",		36,	0,   	true,	1,		1,	0 },
	{ "1",		37,	0,   	false,	0,		0,	EINVAL },
	{ "0x33",	0,	0,   	true,	0x33,		4,	0 },
	{ "0x33",	2,	0,   	false,	0,		1,	EDOM },
	{ "0x33",	10,	0,   	false,	0,		1,	EDOM },
	{ "0x33",	16,	0,   	true,	0x33,		4,	0 },
	{ "0x33",	32,	0,   	false,	0,		1,	EDOM },
	{ "0x33",	34,	0,   	true,	38253,		4,	0 },
	{ "0x33",	36,	0,   	true,	42879,		4,	0 },
	{ "-13a2jH81",	21,	0,   	false,	0,		0,	EBADMSG },
	{ "+015V22um",	35,	0,   	true,	0x7fffffff,	9,	0 },
	{ " 0",		10,	0,   	false,	0,		0,	EBADMSG },
	{ "0 ",		10,	0,   	false,	0,		1,	EBADMSG },
	{ " 0 ",	10,	0,   	false,	0,		0,	EBADMSG },
	{ "0a",		10,	0,   	false,	0,		1,	EDOM },
	{ "123456",	10,	0,   	true,	123456,		6,	0 },
	{ "+123456",	10,	0,   	true,	123456,		7,	0 },
	{ " +123456",	10,	0,   	false,	0,		0,	EBADMSG },
	{ "-123456 ",	10,	0,   	false,	0,		0,	EBADMSG },
	{ "123 456",	10,	0,   	false,	0,		3,	EBADMSG },
	{ "123_456",	10,	0,   	false,	0,		3,	EBADMSG },
	{ "123a456",	10,	0,   	false,	0,		3,	EDOM },
	{ " 0",		10,	W,   	true,	0,		2,	0 },
	{ "0 ",		10,	W,   	true,	0,		2,	0 },
	{ " 0 ",	10,	W,   	true,	0,		3,	0 },
	{ " +0 ",	10,	W,   	true,	0,		4,	0 },
	{ " -0 ",	10,	W,   	false,	0,		1,	EBADMSG },
	{ " -0 1",	10,	W,   	false,	0,		1,	EBADMSG },
	{ " -0 a ",	10,	W,   	false,	0,		1,	EBADMSG },
	{ "0b1",	10,	0,   	false,	0,		1,	EDOM },
	{ "0B1",	10,	0,   	false,	0,		1,	EDOM },
	{ "0o1",	10,	0,   	false,	0,		1,	EDOM },
	{ "0O1",	10,	0,   	false,	0,		1,	EDOM },
	{ "0x1",	10,	0,   	false,	0,		1,	EDOM },
	{ "0X1",	10,	0,   	false,	0,		1,	EDOM },
	{ "0b1",	2,	0,   	false,	0,		1,	EDOM },
	{ "0B1",	2,	0,   	false,	0,		1,	EDOM },
	{ "0o1",	2,	0,   	false,	0,		1,	EDOM },
	{ "0O1",	2,	0,   	false,	0,		1,	EDOM },
	{ "0x1",	2,	0,   	false,	0,		1,	EDOM },
	{ "0X1",	2,	0,   	false,	0,		1,	EDOM },
	{ "0b1",	8,	0,   	false,	0,		1,	EDOM },
	{ "0B1",	8,	0,   	false,	0,		1,	EDOM },
	{ "0o1",	8,	0,   	false,	0,		1,	EDOM },
	{ "0O1",	8,	0,   	false,	0,		1,	EDOM },
	{ "0x1",	8,	0,   	false,	0,		1,	EDOM },
	{ "0X1",	8,	0,   	false,	0,		1,	EDOM },
	{ "0b1",	16,	0,   	true,	0xb1,		3,	0 },
	{ "0B1",	16,	0,   	true,	0xb1,		3,	0 },
	{ "0o1",	16,	0,   	false,	0,		1,	EDOM },
	{ "0O1",	16,	0,   	false,	0,		1,	EDOM },
	{ "0x1",	16,	0,   	true,	1,		3,	0 },
	{ "0X1",	16,	0,   	true,	1,		3,	0 },
	{ "0b1",	36,	0,   	true,	397,		3,	0 },
	{ "0B1",	36,	0,   	true,	397,		3,	0 },
	{ "0o1",	36,	0,   	true,	865,		3,	0 },
	{ "0O1",	36,	0,   	true,	865,		3,	0 },
	{ "0x1",	36,	0,   	true,	1189,		3,	0 },
	{ "0X1",	36,	0,   	true,	1189,		3,	0 },
	{ "0b1",	0,	0,   	false,	0,		1,	EDOM },
	{ "0B1",	0,	0,   	false,	0,		1,	EDOM },
	{ "0o1",	0,	0,   	false,	0,		1,	EDOM },
	{ "0O1",	0,	0,   	false,	0,		1,	EDOM },
	{ "0x1",	0,	0,   	true,	1,		3,	0 },
	{ "0X1",	0,	0,   	true,	1,		3,	0 },
	{ "0b1",	0,	E,   	true,	1,		3,	0 },
	{ "0B1",	0,	E,   	true,	1,		3,	0 },
	{ "0o1",	0,	E,   	true,	1,		3,	0 },
	{ "0O1",	0,	E,   	true,	1,		3,	0 },
	{ "0x1",	0,	E,   	true,	1,		3,	0 },
	{ "0X1",	0,	E,   	true,	1,		3,	0 },
	{ "0b1",	2,	E,   	true,	1,		3,	0 },
	{ "0B1",	2,	E,   	true,	1,		3,	0 },
	{ "0o1",	2,	E,   	false,	0,		1,	EDOM },
	{ "0O1",	2,	E,   	false,	0,		1,	EDOM },
	{ "0x1",	2,	E,   	false,	0,		1,	EDOM },
	{ "0X1",	2,	E,   	false,	0,		1,	EDOM },
	{ "0b1",	8,	E,   	false,	0,		1,	EDOM },
	{ "0B1",	8,	E,   	false,	0,		1,	EDOM },
	{ "0o1",	8,	E,   	true,	1,		3,	0 },
	{ "0O1",	8,	E,   	true,	1,		3,	0 },
	{ "0x1",	8,	E,   	false,	0,		1,	EDOM },
	{ "0X1",	8,	E,   	false,	0,		1,	EDOM },
	{ "0b1",	10,	E,   	false,	0,		1,	EDOM },
	{ "0B1",	10,	E,   	false,	0,		1,	EDOM },
	{ "0o1",	10,	E,   	false,	0,		1,	EDOM },
	{ "0O1",	10,	E,   	false,	0,		1,	EDOM },
	{ "0x1",	10,	E,   	false,	0,		1,	EDOM },
	{ "0X1",	10,	E,   	false,	0,		1,	EDOM },
	{ "0b1",	16,	E,   	true,	0xb1,		3,	0 },
	{ "0B1",	16,	E,   	true,	0xb1,		3,	0 },
	{ "0o1",	16,	E,   	false,	0,		1,	EDOM },
	{ "0O1",	16,	E,   	false,	0,		1,	EDOM },
	{ "0x1",	16,	E,   	true,	1,		3,	0 },
	{ "0X1",	16,	E,   	true,	1,		3,	0 },
	{ "0b1",	36,	E,   	true,	397,		3,	0 },
	{ "0B1",	36,	E,   	true,	397,		3,	0 },
	{ "0o1",	36,	E,   	true,	865,		3,	0 },
	{ "0O1",	36,	E,   	true,	865,		3,	0 },
	{ "0x1",	36,	E,   	true,	1189,		3,	0 },
	{ "0X1",	36,	E,   	true,	1189,		3,	0 },
	{ "07",		0,	0,   	true,	7,		2,	0 },
	{ "07",		0,	O,   	true,	7,		2,	0 },
	{ "09",		0,	0,   	false,	0,		1,	EDOM },
	{ "09",		0,	O,   	true,	9,		2,	0 },
	{ "\t\n075 ",	0,	W|E|O,	true,	75,		6,	0 },
	{ "\t\n075 ",	0,	W|E,	true,	075,		6,	0 },
	{ "\t\n0O75 ",	0,	W|E|O,	true,	075,		7,	0 },
	{ "",		10,	0,	false,	0,		0,	EBADMSG },
	{ "a",		36,	0,	true,	10,		1,	0 },
	{ "b",		36,	0,	true,	11,		1,	0 },
	{ "c",		36,	0,	true,	12,		1,	0 },
	{ "d",		36,	0,	true,	13,		1,	0 },
	{ "e",		36,	0,	true,	14,		1,	0 },
	{ "f",		36,	0,	true,	15,		1,	0 },
	{ "g",		36,	0,	true,	16,		1,	0 },
	{ "h",		36,	0,	true,	17,		1,	0 },
	{ "i",		36,	0,	true,	18,		1,	0 },
	{ "j",		36,	0,	true,	19,		1,	0 },
	{ "k",		36,	0,	true,	20,		1,	0 },
	{ "l",		36,	0,	true,	21,		1,	0 },
	{ "m",		36,	0,	true,	22,		1,	0 },
	{ "n",		36,	0,	true,	23,		1,	0 },
	{ "o",		36,	0,	true,	24,		1,	0 },
	{ "p",		36,	0,	true,	25,		1,	0 },
	{ "q",		36,	0,	true,	26,		1,	0 },
	{ "r",		36,	0,	true,	27,		1,	0 },
	{ "s",		36,	0,	true,	28,		1,	0 },
	{ "t",		36,	0,	true,	29,		1,	0 },
	{ "u",		36,	0,	true,	30,		1,	0 },
	{ "v",		36,	0,	true,	31,		1,	0 },
	{ "w",		36,	0,	true,	32,		1,	0 },
	{ "x",		36,	0,	true,	33,		1,	0 },
	{ "y",		36,	0,	true,	34,		1,	0 },
	{ "z",		36,	0,	true,	35,		1,	0 },
	{ "A",		36,	0,	true,	10,		1,	0 },
	{ "B",		36,	0,	true,	11,		1,	0 },
	{ "C",		36,	0,	true,	12,		1,	0 },
	{ "D",		36,	0,	true,	13,		1,	0 },
	{ "E",		36,	0,	true,	14,		1,	0 },
	{ "F",		36,	0,	true,	15,		1,	0 },
	{ "G",		36,	0,	true,	16,		1,	0 },
	{ "H",		36,	0,	true,	17,		1,	0 },
	{ "I",		36,	0,	true,	18,		1,	0 },
	{ "J",		36,	0,	true,	19,		1,	0 },
	{ "K",		36,	0,	true,	20,		1,	0 },
	{ "L",		36,	0,	true,	21,		1,	0 },
	{ "M",		36,	0,	true,	22,		1,	0 },
	{ "N",		36,	0,	true,	23,		1,	0 },
	{ "O",		36,	0,	true,	24,		1,	0 },
	{ "P",		36,	0,	true,	25,		1,	0 },
	{ "Q",		36,	0,	true,	26,		1,	0 },
	{ "R",		36,	0,	true,	27,		1,	0 },
	{ "S",		36,	0,	true,	28,		1,	0 },
	{ "T",		36,	0,	true,	29,		1,	0 },
	{ "U",		36,	0,	true,	30,		1,	0 },
	{ "V",		36,	0,	true,	31,		1,	0 },
	{ "W",		36,	0,	true,	32,		1,	0 },
	{ "X",		36,	0,	true,	33,		1,	0 },
	{ "Y",		36,	0,	true,	34,		1,	0 },
	{ "Z",		36,	0,	true,	35,		1,	0 },
	{ "01",		0,	0,	true,	1,		2,	0 },
	{ "01",		0,	B,	true,	1,		2,	0 },
	{ "001",	0,	0,	true,	1,		3,	0 },
	{ "001",	0,	B,	false,	0,		1,	EBADMSG },
	{ "0x001",	0,	0,	true,	1,		5,	0 },
	{ "0x001",	0,	B,	false,	0,		3,	EBADMSG },
	{ "1",		0,	B|W,	false,	0,		0,	EINVAL }
};

static void test_table(void)
{
	size_t i;
	bool r;
	char *p;
	ptrdiff_t endptr;
	unsigned long v;

	for (i = 0; i < countof(tests); i++) {
		errno = 0;
		v = 0;
		p = 0;
		r = strict_strtoul(tests[i].input, &p, tests[i].base, &v, tests[i].flags);
		assert(r == tests[i].result);
		assert(v == tests[i].value);
		assert(errno == tests[i].err);
		endptr = p-tests[i].input;
		assert(endptr == tests[i].endptr);
	}
}

static void test_reversible(unsigned long v)
{
	bool r;
	char buff[BUFF_SIZE];
	char *endptr;
	int count;
	unsigned long v1, v2;

	count = snprintf(buff, BUFF_SIZE, "%lu", v);
	errno = 0;
	endptr = 0;
	v1 = strtoul(buff, &endptr, 10);
	assert(errno == 0);
	assert(buff+count == endptr);
	assert(v1 == v);
	errno = 0;
	endptr = 0;
	r = strict_strtoul(buff, &endptr, 10, &v2, 0);
	assert(r);
	assert(errno == 0);
	assert(buff+count == endptr);
	assert(v2 == v);
}

static void test_limits(void)
{
	unsigned long i;

	for (i = 0; i < 1000; i++) {
		test_reversible(i);
	}

	for (i = ULONG_MAX; i >= ULONG_MAX-1000; i--) {
		test_reversible(i);
	}
}

static void test_overflow(void)
{
	bool r;
	char buff[4096];
	unsigned long v;
	size_t i;

	/* TODO: test off-by-one overflow */

	for (i = 0; i < sizeof(buff)-1; i++) {
		buff[i] = 'z';
	}

	buff[sizeof(buff)-1] = '\0';

	v = 0;
	errno = 0;
	r = strict_strtoul(buff, NULL, 36, &v, 0);
	assert(!r);
	assert(errno == EOVERFLOW);
	assert(v == ULONG_MAX);

	v = 0;
	errno = 0;
	r = strict_strtoul(buff, NULL, 36, &v, STRTOL_ALLOW_SATURATE);
	assert(r);
	assert(errno == 0);
	assert(v == ULONG_MAX);

	buff[0] = '-';

	v = 0;
	errno = 0;
	r = strict_strtoul(buff, NULL, 36, &v, 0);
	assert(!r);
	assert(errno == EBADMSG);
	assert(v == 0);

	v = 0;
	errno = 0;
	r = strict_strtoul(buff, NULL, 36, &v, STRTOL_ALLOW_SATURATE);
	assert(!r);
	assert(errno == EBADMSG);
	assert(v == 0);

	buff[sizeof(buff)-2] = '$';

	v = 0;
	errno = 0;
	r = strict_strtoul(buff, NULL, 36, &v, STRTOL_ALLOW_SATURATE);
	assert(!r);
	assert(errno == EBADMSG);
	assert(v == 0);
}

int main(void)
{
	test_table();
	test_limits();
	test_overflow();

	return 0;
}
